# Search client for the special collections at uib

This repo contains branches for different search clients for marcus. All the branches act as the main branch, for their web-page. 
It has two remotes, one in git.app.uib.no, and one in itgit.app.uib.no for different deployments.

* **marcus** [branch](https://git.app.uib.no/uib-ub/spesialsamlingene/marcus-search-client/-/tree/marcus) (https://marcus.uib.no/search)
* **admin** [branch](https://git.app.uib.no/uib-ub/spesialsamlingene/marcus-search-client/-/tree/admin) (https://admin.marcus.uib.no/search/
* **naturen** [branch](https://git.app.uib.no/uib-ub/spesialsamlingene/marcus-search-client/-/tree/naturen) (https://marcus.uib.no/search)
* **ska** [branch](https://git.app.uib.no/uib-ub/spesialsamlingene/marcus-search-client/-/tree/ska) (https://katalog.skeivtarkiv.no/search)


`Master` is defined as the main branch to avoid accidental/wrong merge requests. All files are removed from the `master` branch, and it is protected from pushing or merging to it.

The branches `marcus`, `admin`,`ska' and `naturen` contains CI deployment to test and prod. 
`ska` prod deployment has to be initialized from itgit.app.uib.no due to firewall restrictions.


A possible workflow is: 
* [open an issue](https://git.app.uib.no/uib-ub/spesialsamlingene/marcus-search-client/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)
* create merge request (from issue in [web-GUI](https://git.app.uib.no/uib-ub/spesialsamlingene/marcus-components/-/issues)) with source set to *marcus*,*admin*,*naturen* or *ska*
![Example of creating a merge request from marcus branch](docs/mr-from-marcus.png)
* push to merge request
* review/confirm changes/feature in corresponding test servers ([https://marcus.test.uib.no/search](https://marcus.test.uib.no/search),[https://admin.marcus.ubbe.no/search](https://admin.marcus.ubbe.no/search), [https://marcus.uib.no/naturen](https://test.marcus.uib.no/naturen))
* merge request to `marcus`, `naturen` or `admin`
* Deploy to the production servers ([https://marcus.uib.no/search](https://marcus.uib.no/search),[https://marcus.uib.no/naturen](https://marcus.uib.no/naturen),[https://admin.marcus.uib.no/search](https://admin.marcus.uib.no/search])) by clicking on the `deploy_prod` button after the `build` and `deploy` jobs has succesfully completed for the `naturen`, `marcus` or `admin` branch.

![Image of deployment](docs/search-deploy.png)

There also exists a fork of this repo at https://git.app.uib.no/wab/sfb-search-client following a similar workflow
